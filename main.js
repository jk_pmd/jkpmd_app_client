import Vue from 'vue'
import App from './App'
import uView from "uview-ui";
import GoEasy from "./goeasy/goeasy-1.2.1.js"
import store from './store/index.js'
Vue.prototype.$store = store
Vue.prototype.$goEasy = GoEasy.getInstance({
      host:'hangzhou.goeasy.io', //应用所在的区域地址: 【hangzhou.goeasy.io |singapore.goeasy.io】
      appkey: "PC-81919a5b06774be4a1a5baf98d36339a",//替换为您的应用appkey BC-8ec70f4f88654be6ba9890c8ab9e06ea
	  allowNotification:true,
});
Vue.use(uView);
Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	store,
    ...App
})

import httpInterceptor from '@/common/http.interceptor.js'
// 这里需要写在最后，是为了等Vue创建对象完成，引入"app"对象(也即页面的"this"实例)
Vue.use(httpInterceptor, app)

// http接口API集中管理引入部分
import httpApi from '@/common/http.api.js'
Vue.use(httpApi, app)

app.$mount()
