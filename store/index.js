import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
import baseUrl from '../common/baseInfo.js'
const store = new Vuex.Store({
	state:{
		islogin:false,
		userInfo:{
			
		}
	},
	mutations:{
		login(state,data){
			//console.log(data)
			state.islogin = true,
			state.userInfo = data,
			uni.setStorage({
				key:'userInfo',
				data:data
			})
			console.log(state)
		},
		logout(state){
			state.islogin = false,
			state.userInfo = {}
			uni.removeStorage({
				key:'userInfo'
			})
			//console.log('logout')
		}
	},
	actions:{
		getNewUserInfo({ commit },data){
			uni.request({
			    url: `${baseUrl}/user/api/getDoctor/${data.id}`, //仅为示例，并非真实接口地址。
			    success: (res) => {
			        console.log(res);
					if(res.data.code == 0){
						commit('login',res.data.data[0])
					}
			    }
			});
			/* console.log(this,222)
			this.$u.get(`${this.$u.http.config.baseUrl}/user/api/getDoctor/${data.id}`).then(res=>{
				console.log(res);
				
			}) */
		}
	}
})
export default store