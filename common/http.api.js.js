// /common/http.api.js

// 如果没有通过拦截器配置域名的话，可以在这里写上完整的URL(加上域名部分)
/* let hotSearchUrl = '/ebapi/store_api/hot_search';
let indexUrl = '/ebapi/public_api/index'; */

// 此处第二个参数vm，就是我们在页面使用的this，你可以通过vm获取vuex等操作，更多内容详见uView对拦截器的介绍部分：
// https://uviewui.com/js/http.html#%E4%BD%95%E8%B0%93%E8%AF%B7%E6%B1%82%E6%8B%A6%E6%88%AA%EF%BC%9F
const install = (Vue, vm) => {
	let login = (params = {},header={}) => vm.$u.post('/user/api/login',params,header)//登陆
	let insertLoginLog = (params = {},header={}) => vm.$u.post('/login/insertLoginLog',params,header)//插入用户登陆记录表
	let getCardTime = (params = {}) => vm.$u.get(`/upload/checkUserJKCard/`,params)//获取用户精康检测卡剩余次数
	let insertResultGroup =(params = {},header={}) => vm.$u.post('/upload/insertResultGroup',params,header) //开始计算
	let updateResult =(params ={}) => vm.$u.post('/upload/updateResult',params) //插入计算结果
	let reduceJKcard = (params = {}) => vm.$u.post('/iccard/api/reduceNumber',params) //精康检测卡次数-1
	
	let getResultGro =(params={}) => vm.$u.get('/upload/getResultGro',params)  
	let getPath =(params={}) => vm.$u.get('/upload/getPath',params)
	let getResultInfo =(params={},header={}) => vm.$u.post('/upload/getText',params,header)
	
	let registerOrUpdate = (params={},header={}) => vm.$u.post('/user/api/registerOrUpdate',params,header) //注册以及信息修改，昵称修改，密码修改
	let checkPhone = (params={},header={}) => vm.$u.post('/login/checkUsername',params,header) //校验账号是否存在
	let findPwd =(params={},header={}) => vm.$u.put('/user/api/forgetPassword',params,header)  //找回密码
	
	let getAllDoctors = (params={}) => vm.$u.get('/user/api/getDoctorsList',params) //获取医生列表
	let getMyDoctors = (params={}) => vm.$u.get('/userfans/api/getFansByUserId',params) //获取用户关注的医生列表
	let focusAndCancelFoucus = (params={},header={}) => vm.$u.post('/userfans/api/followAndUnFollow',params,header) //关注与取消关注
	
	let getNewsList = (params={}) => vm.$u.get('/news',params) //获取新闻列表	
	
	let addMessage = (params={},header={}) => vm.$u.post('/comment/leaveAMessage',params,header) //提交留言
	let getMessages = (params={}) => vm.$u.get('/comment/getMessageListById',params) //查询我的留言列表
	let getMessageInfo = (params={}) => vm.$u.get('/comment/getSingleMessageListById',params) //查询两人之间具体聊天信息
	let checkPermissions = (params={}) => vm.$u.get('/order/checkDeadlineByUserId',params) //校验用户是否有留言权限
	let appleNotify = (params={},header={}) => vm.$u.put('/order/appleNotify',params,header) //苹果支付成功后开通用户聊天服务的回调
	
	let getOtp = (params={}) => vm.$u.get('/user/api/getOTP',params) //获取OTP
	// 将各个定义的接口名称，统一放进对象挂载到vm.$u.api(因为vm就是this，也即this.$u.api)下
	vm.$u.api = {login, insertLoginLog, getCardTime, insertResultGroup, updateResult, reduceJKcard, getResultGro, 
	getPath, getResultInfo, registerOrUpdate, findPwd, checkPhone, addMessage, getMessages, getMessageInfo,
	getAllDoctors, getMyDoctors, focusAndCancelFoucus, checkPermissions, getNewsList, appleNotify, getOtp};
}

export default {
	install
}